package co.com.mudanza.services.exceptions;

import lombok.Getter;
import lombok.Setter;

// TODO: Auto-generated Javadoc
/**
 * The Class MudanzaServiceException is an exception that indicates any business error generated.
 */

/* (non-Javadoc)
 * @see java.lang.Throwable#getMessage()
 */
@Getter

/**
 * Sets the message.
 *
 * @param message the new message
 */
@Setter
public class MudanzaServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The code. */
	private int code;
	
	/** The message. */
	private String message;
	
	/**
	 * Instantiates a new mudanza service exception.
	 */
	public MudanzaServiceException() {
		
	}
	
	/**
	 * Instantiates a new mudanza service exception.
	 *
	 * @param code the code
	 * @param message the message
	 */
	public MudanzaServiceException(final int code, final String message) {
		super(message);
		this.code = code;
		this.message = message;
	}
	
	/**
	 * Instantiates a new mudanza service exception.
	 *
	 * @param code the code
	 * @param message the message
	 * @param cause the cause
	 */
	public MudanzaServiceException(final int code, final String message, final Throwable cause) {
		super(message, cause);
		this.code = code;
		this.message = message;
	}
	
	/**
	 * Instantiates a new mudanza service exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public MudanzaServiceException(final String message, final Throwable cause) {
		super(message, cause);
		this.message = message;
	}


	
	
}
