package co.com.mudanza.services.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class GeneralRequest is a base class that implements a generic type for
 * request detail.
 *
 * @param <T>
 *            the generic type
 */
@Getter
@Setter
public class GeneralRequest<T> {

	/** The transaction id is a attribute that contains an UUID code. */
	private String transactionId;
	
	/** The payload request detailed. */
	private T payload;
	
}
