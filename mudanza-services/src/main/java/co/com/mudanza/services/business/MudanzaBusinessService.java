package co.com.mudanza.services.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.mudanza.services.dto.AuditDto;
import co.com.mudanza.services.dto.GeneralResponse;
import co.com.mudanza.services.dto.MaxTravelDto;
import co.com.mudanza.services.enums.MudanzaResponseEnum;
import co.com.mudanza.services.exceptions.MudanzaServiceException;
import co.com.mudanza.services.jpa.model.Audit;
import co.com.mudanza.services.jpa.repositories.AuditJpaRepository;
import co.com.mudanza.services.utils.EntityMapperUtil;

/**
 * The Class MudanzaBusinessService is a class that contains the main functions
 * for calculate jobs and stores the log information.
 */
@Service
public class MudanzaBusinessService {

	/** The Constant MIN_WEIGHT. */
	private static final int MIN_WEIGHT = 50;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(MudanzaBusinessService.class);
	
	/** The Constant T. */
	private static final Integer T = 500;
	
	/** The Constant N. */
	private static final Integer N = 100;
	
	/** The Constant CASE_TMP. */
	private static final String CASE_TMP = "Case #%S: ";
	
	/** The audit jpa repository. */
	@Autowired
	AuditJpaRepository auditJpaRepository;
	
	
	/**
	 * Method responsible to calculates maximum jobs
	 *
	 * @param request the request
	 * @param transactionId the transaction id
	 * @return the general response
	 * @throws MudanzaServiceException the mudanza service exception
	 */
	public GeneralResponse<String> calculateMazxTravels(MaxTravelDto request, String transactionId) throws MudanzaServiceException {
		
		GeneralResponse<String> response = validateInput(request);
		
		if(response != null)
			return response;
		
		Map<Integer, List<Integer>> execution = buildMap(request.getInputData());
		
		response = validateRestriccionsByDay(execution);
		if(response != null)
			return response;
		
		auditJpaRepository.save(buildAudit(request.getDocument(), transactionId));
		
		return generateResponse(MudanzaResponseEnum.SUCCESS.getCode(),
				MudanzaResponseEnum.SUCCESS.getMessage(), generateOutputFile(execution));
	}
	
	/**
	 * Gets the audit.
	 *
	 * @param document the document
	 * @return the audit
	 */
	public GeneralResponse<List<AuditDto>> getAudit(final String document){
		GeneralResponse<List<AuditDto>> response = new GeneralResponse<>();
		List<AuditDto> detail = new ArrayList<>();
		
		List<Audit> auditList = auditJpaRepository.findByDocument(document);
		
		if(auditList.isEmpty()) {
			response.setCode(MudanzaResponseEnum.DATA_NOT_FOUND.getCode());
			response.setDescription(String.format(MudanzaResponseEnum.DATA_NOT_FOUND.getMessage(), "Auditoría"));
			return response;
		}
		
		auditList.stream().forEach(a -> detail.add(EntityMapperUtil.toAuditDto(a)));
		
		response.setCode(MudanzaResponseEnum.SUCCESS.getCode());
		response.setDescription(MudanzaResponseEnum.SUCCESS.getMessage());
		response.setDetail(detail);
		
		return response;
	}
	
	/**
	 * Generate output file.
	 *
	 * @param execution the execution
	 * @return the string
	 * @throws MudanzaServiceException the mudanza service exception
	 */
	private String generateOutputFile(Map<Integer, List<Integer>> execution) throws MudanzaServiceException {
		
		final Map<String, Integer> result = new HashMap<>();		
		execution.entrySet().stream().forEach(e -> result.put(String.format(CASE_TMP, e.getKey()), calculateJob(e.getValue())));
		String binaryResult = null;
		
		try {
			final File file = new File("lazy_loading_example_output.txt");
			OutputStream fos = new FileOutputStream(file);
			final PrintWriter pw = new PrintWriter(fos);
		    
		    result.entrySet().stream().forEach(r -> pw.println(r.getKey() + r.getValue()));
		    pw.flush();
			pw.close();
			fos.close();
			
			byte[] binaryData = FileUtils.readFileToByteArray(file);
			
			binaryResult = StringUtils.newStringUtf8(Base64.encodeBase64(binaryData));
			
			    
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new MudanzaServiceException(MudanzaResponseEnum.FILED_FILE.getCode(),
					MudanzaResponseEnum.FILED_FILE.getMessage(), e);
		}
	    
		 
		return binaryResult;
	}
	
	/**
	 * Calculate job.
	 *
	 * @param elements the elements
	 * @return the integer
	 */
	private Integer calculateJob(List<Integer> elements) {
		
		Integer jobs = 1;
		List<Integer> elementsOrdered = elements.stream().sorted(Comparator.naturalOrder())
				.collect(Collectors.toList());
		
		Integer maxWaight = elements.stream().max(Comparator.naturalOrder()).get();
		Integer numElements = (int) Math.round((MIN_WEIGHT / maxWaight) + 0.5);
		elementsOrdered.remove(elementsOrdered.size() - 1);
		
		for(int i = 1; i < numElements; i++) {
			if(elementsOrdered.isEmpty())
				break;
			elementsOrdered.remove(0);
		}
		
		if(!elementsOrdered.isEmpty())
			jobs += calculateJob(elementsOrdered);
		
		
		return jobs;
	}
	
	/**
	 * Builds the audit.
	 *
	 * @param document the document
	 * @param transactionId the transaction id
	 * @return the audit
	 */
	private Audit buildAudit(final String document,final String transactionId) {
		final Audit audit = new Audit();
		audit.setDocument(document);
		audit.setTransactionId(transactionId);
		audit.setExecutionDate(LocalDateTime.now());
		
		return audit;
	}
	
	/**
	 * Validate restriccions by day.
	 *
	 * @param execution the execution
	 * @return the general response
	 */
	private GeneralResponse<String> validateRestriccionsByDay(Map<Integer, List<Integer>> execution){
		
		GeneralResponse<String> responseValidate = null;
		
		List<Integer> invalidNumber = execution.entrySet().stream()
				.filter(getInvalidNumbers(N))
				.map(Map.Entry :: getValue)
				.flatMap(List :: stream).collect(Collectors.toList());
		
		if(!invalidNumber.isEmpty())
			responseValidate = generateResponse(MudanzaResponseEnum.WEIGHT_EXCEEDS.getCode(),
					MudanzaResponseEnum.WEIGHT_EXCEEDS.getMessage(), null);
		
		return responseValidate;
	}
	
	/**
	 * Gets the invalid numbers.
	 *
	 * @param max the max
	 * @return the invalid numbers
	 */
	private Predicate<Map.Entry<Integer, List<Integer>>> getInvalidNumbers(Integer max){
		return maxNumber -> maxNumber.getKey() > max;
	}

	/**
	 * Builds the map.
	 *
	 * @param inputData the input data
	 * @return the map
	 * @throws MudanzaServiceException the mudanza service exception
	 */
	private Map<Integer, List<Integer>> buildMap(List<Integer> inputData) throws MudanzaServiceException {
		Map<Integer, List<Integer>> execution = new HashMap<>();
		int days = inputData.remove(0);
		
		int initPos = 0;
		int finalPos = 0;
		for(int i = 1; i <= days; i++) {
			
			if(inputData.size() <= initPos || inputData.size() <= finalPos) 
				throw new MudanzaServiceException(5, "La cantidad de elementos a cargar no coincide");
			
			if(inputData.get(initPos) > N) 
				throw new MudanzaServiceException(MudanzaResponseEnum.ELEMENTS_EXCEEDS.getCode(),
						MudanzaResponseEnum.ELEMENTS_EXCEEDS.getMessage());
			
			finalPos = inputData.get(initPos) + initPos;
			execution.put(i, inputData.subList(initPos + 1, finalPos + 1));
			initPos = finalPos + 1;
		}
		
		return execution;
	}
	
	/**
	 * Validate input.
	 *
	 * @param request the request
	 * @return the general response
	 */
	private GeneralResponse<String> validateInput(MaxTravelDto request){
		
		GeneralResponse<String> response = null;
		List<Integer> inputData = request.getInputData();
		
		if(inputData.isEmpty()) {
			return generateResponse(MudanzaResponseEnum.MISSING_REQUEST.getCode(),
					MudanzaResponseEnum.MISSING_REQUEST.getMessage(), null);
		}
		
		if(inputData.get(0) > T) {
			response = generateResponse(MudanzaResponseEnum.WORK_EXCEEDS.getCode(),
					MudanzaResponseEnum.WORK_EXCEEDS.getMessage(), null);
		}
		
		return response;
	}
	
	/**
	 * Generate response.
	 *
	 * @param code the code
	 * @param description the description
	 * @param detail the detail
	 * @return the general response
	 */
	private GeneralResponse<String> generateResponse(final int code, final String description, final String detail) {
		final GeneralResponse<String> response = new GeneralResponse<>();
		response.setCode(code);
		response.setDescription(description);
		response.setDetail(detail);

		return response;
	}
}
