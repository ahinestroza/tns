package co.com.mudanza.services.utils;

import java.time.format.DateTimeFormatter;

import co.com.mudanza.services.dto.AuditDto;
import co.com.mudanza.services.jpa.model.Audit;

public class EntityMapperUtil {

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
	
	public static final AuditDto toAuditDto(final Audit entity) {
		final AuditDto dto = new AuditDto();
		dto.setId(entity.getId());
		dto.setTransactionId(entity.getTransactionId());
		dto.setDocument(entity.getDocument());
		dto.setExecutionDate(entity.getExecutionDate().format(formatter));
		
		return dto;
	}
}
