package co.com.mudanza.services.enums;

import lombok.Getter;

@Getter
public enum MudanzaResponseEnum {

	SUCCESS(0, "Operación exitosa"),
	MISSING_REQUEST(1, "No se ha encontrado datos de entrada"),
	WORK_EXCEEDS(2, "La cantidad de días trabajados no puede ser superior a 500"),
	ELEMENTS_EXCEEDS(3, "La cantidad de elementos a transportar no puede ser superior a 100"),
	WEIGHT_EXCEEDS(4, "El peso de cada elemento no puede ser superior a 100"),
	DATA_NOT_FOUND(5, "No se encontró información para la %S"),
	FILED_FILE(6, "Error creando el archivo de salida");
	
	
	private int code;
	private String message;
	
	private MudanzaResponseEnum(final int code, final String message) {
		this.code = code;
		this.message = message;
	}
	
}
