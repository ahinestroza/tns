package co.com.mudanza.services.jpa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.mudanza.services.jpa.model.Audit;

public interface AuditJpaRepository extends JpaRepository<Audit, Integer> {

	@Query("select a from Audit a where a.document =:DOCUMENT order by a.executionDate desc")
	List<Audit> findByDocument(@Param("DOCUMENT") String document);
}
