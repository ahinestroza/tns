package co.com.mudanza.services.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MaxTravelDto {

	private String document;
	private List<Integer> inputData;
}
