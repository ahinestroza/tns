package co.com.mudanza.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * The Class CallCenterApplication is responsible to starts the application.
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories
public class MudanzaApplication {

	/**
	 * Main method that starts the spring application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(MudanzaApplication.class, args);
	}
}
