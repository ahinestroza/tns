package co.com.mudanza.services.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.mudanza.services.business.MudanzaBusinessService;
import co.com.mudanza.services.dto.AuditDto;
import co.com.mudanza.services.dto.GeneralRequest;
import co.com.mudanza.services.dto.GeneralResponse;
import co.com.mudanza.services.dto.MaxTravelDto;
import co.com.mudanza.services.exceptions.MudanzaServiceException;

/**
 * The Class MudanzaController is a controller responsible to exposes the Rest API Services.
 */
@CrossOrigin()
@RestController
public class MudanzaController {
	
	/** The mudanza business service. */
	@Autowired
	private MudanzaBusinessService mudanzaBusinessService;
	
	/**
	 * Creates the employee.
	 *
	 * @param request the request
	 * @return the response entity
	 * @throws MudanzaServiceException the mudanza service exception
	 */
	@RequestMapping(method = RequestMethod.POST, path="/mudanza/services/calculateTravels")
	public ResponseEntity<GeneralResponse<String>> createEmployee(@RequestBody final GeneralRequest<MaxTravelDto> request) throws MudanzaServiceException{
		
		final GeneralResponse<String> response = mudanzaBusinessService.calculateMazxTravels(request.getPayload(), request.getTransactionId());
		
		return new ResponseEntity<GeneralResponse<String>>(response, HttpStatus.OK);
	}
	
	/**
	 * Gets the audit.
	 *
	 * @param document the document
	 * @return the audit
	 */
	@GetMapping("/mudanza/services/getAudit/{document}")
	public GeneralResponse<List<AuditDto>> getAudit(@PathVariable final String document){
		return mudanzaBusinessService.getAudit(document);
	}
}
