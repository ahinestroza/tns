package co.com.mudanza.services.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuditDto {

	private Integer id;
	private String transactionId;
	private String document;
	private String executionDate;
}
