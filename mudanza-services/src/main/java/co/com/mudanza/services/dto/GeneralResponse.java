package co.com.mudanza.services.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class GeneralResponse is a base class that implements a generic type for detail.
 *
 * @param <T> the generic type
 */
@Getter
@Setter
public class GeneralResponse<T> {

	/** The code response. */
	private int code;
	
	/** The description response. */
	private String description;
	
	/** The detail response. */
	private T detail; 
	
}
