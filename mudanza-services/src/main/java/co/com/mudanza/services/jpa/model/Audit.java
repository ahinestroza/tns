package co.com.mudanza.services.jpa.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Audit is an entity that contains the execution information logs.
 */
@Entity
@Table(name = "audit")
@Getter
@Setter
public class Audit {

	/** The id attribute contains the employee identifier. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/** The transaction id. */
	private String transactionId;
	
	/** The user document that launch execution. */
	private String document;
	
	/** The execution date. */
	private LocalDateTime executionDate;
}
