package co.com.mudanza.services.test.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import co.com.mudanza.services.dto.MaxTravelDto;
import co.com.mudanza.services.jpa.model.Audit;

public final class TestDataProvider {

	private static final String DOCUMENTO = "1234567890";
	
	public static MaxTravelDto buildMaxTravelDto() {
		final MaxTravelDto dto = new MaxTravelDto();
		dto.setDocument(DOCUMENTO);
		
		return dto;
	}
	
	public static List<Audit> buildAuditList(){
		List<Audit> auditList = new ArrayList<>();
		auditList.add(buildAudit());
		
		return auditList;
	}
	
	public static Audit buildAudit() {
		final Audit audit = new Audit();
		audit.setId(1);
		audit.setTransactionId(UUID.randomUUID().toString());
		audit.setDocument(DOCUMENTO);
		audit.setExecutionDate(LocalDateTime.now());
		
		return audit;
	}
}
