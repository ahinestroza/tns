package co.com.mudanza.services.business.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.mudanza.services.business.MudanzaBusinessService;
import co.com.mudanza.services.dto.AuditDto;
import co.com.mudanza.services.dto.GeneralResponse;
import co.com.mudanza.services.dto.MaxTravelDto;
import co.com.mudanza.services.exceptions.MudanzaServiceException;
import co.com.mudanza.services.jpa.model.Audit;
import co.com.mudanza.services.jpa.repositories.AuditJpaRepository;
import co.com.mudanza.services.test.utils.TestDataProvider;

@RunWith(MockitoJUnitRunner.class)
public class MudanzaBusinessServiceTest {

	private static final String DOCUMENTO = "1234567890";

	@InjectMocks
	private MudanzaBusinessService mudanzaBusinessService;
	
	@Mock
	private AuditJpaRepository auditJpaRepository;
	
	private MaxTravelDto maxTravelDto;
	
	private ObjectMapper objectMapper;
	
	@Before
	public void setup() {
		
		objectMapper = new ObjectMapper();
		
		try {
			URI uri = new ClassPathResource("input.json").getURI();
			String jsonInput = new String(Files.readAllBytes(Paths.get(uri)), Charset.forName("utf-8"));
			maxTravelDto = objectMapper.readValue(jsonInput, MaxTravelDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void calculateMazxTravels_validaciones() {
		when(auditJpaRepository.save(any(Audit.class))).thenReturn(new Audit());
		try {
			GeneralResponse<String> response = mudanzaBusinessService.calculateMazxTravels(maxTravelDto, UUID.randomUUID().toString());
			assertNotNull(response);
			assertTrue(response.getCode() == 0);
			verify(auditJpaRepository).save(any(Audit.class));
		} catch (MudanzaServiceException e) {
			fail();
		}
	}
	
	@Test
	public void getAudit_debe_retornar_resultados() {
		when(auditJpaRepository.findByDocument(anyString())).thenReturn(TestDataProvider.buildAuditList());
		
		GeneralResponse<List<AuditDto>> response = mudanzaBusinessService.getAudit(DOCUMENTO);
		assertNotNull(response);
		assertTrue(response.getCode() == 0);
		assertTrue(response.getDetail().size() == 1);
		verify(auditJpaRepository).findByDocument(DOCUMENTO);
	}
}
