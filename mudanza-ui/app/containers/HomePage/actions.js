import * as actionTypes from './constants';

export function defaultAction() {
  return {
    type: actionTypes.DEFAULT_ACTION,
  };
}

export function setFileAction(file) {
  return {
    type: actionTypes.SET_FILE,
    file,
  };
}

export function cargarElementosAction(data) {
  console.log(data);
  return {
    type: actionTypes.CARGAR_ARCHIVO,
    data,
  };
}

export function setRespuestaCargaAction(data) {
    return {
      type: actionTypes.SET_RESPUESTA_CARGA,
      data,
    };
}

export function cleanRespuestaCargaAction() {
  console.log('cleanRespuestaCargaAction');
    return { type: actionTypes.CLEAN_RESPUESTA_CARGA };
}
