import { fromJS } from 'immutable';
import * as TypeAction from './constants';

const initialState = fromJS({
  file: false,
  respuestaCarga: false,
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case TypeAction.DEFAULT_ACTION:
      return state;
    case TypeAction.SET_FILE:
      return state.set('file', action.file);
    case TypeAction.SET_RESPUESTA_CARGA:
      return state.set('respuestaCarga', `data:text/txt;base64,${action.data}`);
    case TypeAction.CLEAN_RESPUESTA_CARGA:
      return state.set('respuestaCarga', false);
    default:
      return state;
  }
}

export default homePageReducer;
