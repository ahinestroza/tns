export const DEFAULT_ACTION = 'app/HomePage/DEFAULT_ACTION';
export const SET_FILE = 'app/HomePage/SET_FILE';
export const CARGAR_ARCHIVO = 'app/HomePage/CARGAR_ARCHIVO';
export const SET_RESPUESTA_CARGA = 'app/HomePage/SET_RESPUESTA_CARGA';
export const CLEAN_RESPUESTA_CARGA = 'app/HomePage/CLEAN_RESPUESTA_CARGA';
