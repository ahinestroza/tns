/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Papa from 'papaparse';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import styles from '../../css/style.css';

import * as actions from './actions';
import Download from '../../components/Download';
import { makeSelectHomePage } from './selectors';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  uploadFile = (e) => {
    if (e.target.files.length === 0) {
      this.props.setFile(false);
      this.props.cleanRespuestaCarga();
    } else {
      this.props.setFile(e.target.files[0]);
    }
  }

  submitUploadWorkFile = (e) => {
    e.preventDefault();
    const request = {
      document: e.target.document.value,
      inputData: [],
    };

    Papa.parse(
      this.props.homePage.file,
      {
        comments: true,
        step: (row) => {
          request.inputData.push(row.data[0].toString());
        },
        complete: () => {
          this.props.cargarElementos(request);
        }
      }
    );
  }

  render() {
    return (
      <div className="container">
        <div className="row">

          <h2 className="title-page"> Cargar archivos de trabajo</h2>
          <form className="load-file" onSubmit={this.submitUploadWorkFile} >

            <div className="form-group col-md-6">
              <div className="btn-out">
                <label htmlFor="text" className="col-form-label">
                  * Digite el número de documento
                  <input type="text" name="document" className="panel-body form-control"/>
                </label>
              </div>
            </div>
            <div className="form-group col-md-6">
              <div className="btn-out">
                <input
                  id="inputTxt"
                  type="file"
                  accept=".txt"
                  onChange={this.uploadFile}
                  className="form-control input-box"
                />
              </div>
            </div>
            <div className="form-group col-md-6">
              <button
                type="submit"
                className="btn btn-out">Cargar Archivo</button>
            </div>
            
          </form>
          <Download
            content={this.props.homePage.respuestaCarga}
            fileName="lazy_loading_example_input.txt"
            cleanFile={this.props.cleanRespuestaCarga}
          />
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  setFile: PropTypes.func,
  cleanRespuestaCarga: PropTypes.func,
  cargarElementos: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  homePage: makeSelectHomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    setFile: (file) => dispatch(actions.setFileAction(file)),
    cleanRespuestaCarga: () => dispatch(actions.cleanRespuestaCargaAction()),
    cargarElementos: (request) => dispatch(actions.cargarElementosAction(request)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose( withReducer, withSaga, withConnect )(HomePage);
