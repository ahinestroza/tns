import { createSelector } from 'reselect';

const selectHomePageDomain = (state) => state.get('homePage');

const makeSelectHomePage = () => createSelector(
  selectHomePageDomain,
  (substate) => substate.toJS()
);

const makeSelecRequest = () => createSelector(
  selectHomePageDomain,
  (substate) => substate.get('request')
);

const makeSelectResponse = () => createSelector(
  selectHomePageDomain,
  (substate) => substate.get('respuestaCarga')
);

export default makeSelectHomePage;
export {
  makeSelectHomePage,
  makeSelecRequest,
};
