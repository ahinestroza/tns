import { LOCATION_CHANGE } from 'react-router-redux';
import { take, call, put, cancel, takeLatest } from 'redux-saga/effects';
import RestAPI from '../../utils/RestAPI';
import * as constantsCp from './constants';
import Endpoints from '../../utils/Endpoints';
import * as actions from './actions';

export function* cargarArchivo(action) {
  try {
    
    const body = { 
      transactionId: 'testing', 
      payload: action.data 
    };

    const response = yield call(RestAPI.doPost, Endpoints.CALCULAR_JOB_SERVICES, body);
    console.log(response);
    yield put(actions.setRespuestaCargaAction(response.detail));
  } catch (error) {
    yield put({ type: 'CARGAR_ERROR', errorMessage: error.message });
  }
}

export default function* defaultSaga() {
  yield takeLatest(constantsCp.CARGAR_ARCHIVO, cargarArchivo);
}
