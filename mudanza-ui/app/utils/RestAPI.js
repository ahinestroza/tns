/* eslint-disable object-property-newline,object-shorthand,no-underscore-dangle,no-param-reassign */
import axios from 'axios';
import {
  SET_INTERNAL_ERROR_MESSAGE,
  CLEAN_INTERNAL_ERROR_MESSAGE,
} from '../containers/App/constants';


/** *****************************************************************************
 * OPERACIONES DISPONIBLES EN EL RESP API
 ******************************************************************************/

const instance = initAxios();


function initAxios() {
  const inst = axios.create({
    baseURL: 'http://localhost:9000',
    timeout: 30000,
  });

  inst.defaults.headers.common.useXDomain = true;

  return inst;
}

function interpolateIfNeeded(url, params) {
  return (params && interpolate(url, params)) || url;
}

function handleServerError(serverData) {
  throw new Error(serverData.response.headers.internalErrorMessage);
}

function baseOptions(method, params, data, transformRequest, transformResponse) {
  const opts = {
    method,
    params: params || {}, data: data || {},
  };
  if (transformRequest) { opts.transformRequest = transformRequest; }
  if (transformResponse) { opts.transformResponse = transformResponse; }
  return opts;
}

function checkStatus(serverResp) {
  if (serverResp.status >= 200 && serverResp.status < 300) {
    return serverResp.data;
  }
  let errorMsg;
  if (serverResp.data && serverResp.data.mensaje) {
    errorMsg = serverResp.data.mensaje;
  }
  errorMsg = errorMsg || (serverResp.response && serverResp.response.headers.internalerrormessage) || resolveErrorMessageByStatus(serverResp);
  throw new Error(errorMsg);
}


function doGet(url, params, transformResponse, customConfig = {}) {
  const config = Object.assign(baseOptions('GET', params, undefined, undefined, transformResponse), customConfig);

  return instance.request(interpolateIfNeeded(url, params), config)
    .then(checkStatus)
    .catch(handleServerError);
}


function doPost(url, data, params, transformRequest, transformResponse, customConfig = {}) {
  const config = Object.assign(baseOptions('POST', params, data, transformRequest, transformResponse), customConfig);
  return instance.request(interpolateIfNeeded(url, params), config)
    .then(checkStatus)
    .catch(handleServerError);
}


export default {
  doGet,
  doPost,
};

