function popKey( object, key ) {
  var value = object[ key ];
  delete( object[ key ] );
  return( value );
}

function popFirstKey( object1, object2, objectN, key ) {
  var objects = Array.prototype.slice.call( arguments );
  key = objects.pop();
  var object = null;
  while ( (object = objects.shift()) ) {
    if ( object.hasOwnProperty( key ) ) {
      return( popKey( object, key ) );
    }
  }
}

// url/base/(:param1/:param2/:param_n)
export function interpolate( url, params, data ) {
  params = ( params || {} );
  data = ( data || {} );
  url = url.replace( /(\(\s*|\s*\)|\s*\|\s*)/g, "" );
  url = url.replace(
    /:([a-z]\w*)/gi,
    function( $0, label ) {
      return( popFirstKey( data, params, label ) || "" );
    }
  );
  url = url.replace( /(^|[^:])[\/]{2,}/g, "$1/" );
  url = url.replace( /\/+$/i, "" );
  return( url );
}
