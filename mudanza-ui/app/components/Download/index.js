
import React from 'react';
import fileDownload from 'downloadjs';

export default class Download extends React.Component {

  componentDidUpdate() {
    if (this.props.content) {
      fileDownload(this.props.content, this.props.fileName);
      this.props.cleanFile();
    }
  }

  render() {
    return (
      <div />
    );
  }
}

Download.propTypes = {
  content: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.bool,
  ]),
  fileName: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.bool,
  ]),
  cleanFile: React.PropTypes.func,
};
