/* eslint-disable react/prop-types,no-useless-constructor */
import React from 'react';
import styled, { keyframes } from 'styled-components';
import Spinner from './img/spinner.gif';

export default class LoaderComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.initTimer();
  }

  componentDidUpdate() {
    if (this.props.tiempoRestanteCargador > 0) {
      setTimeout(() => this.props.decreaseTimer(), 1000);
    } else if (this.props.executingCommand) {
      this.props.finishHttpRequest(this.props.executingCommand);
      this.props.cancelCommandResponseWaiting(this.props.executingCommand);
    }
  }

  render() {
    return (
      <div className="bg-loading">
        <img src={Spinner} alt="Cargando" id="load_indicator" />
      </div>
    );
  }

}

